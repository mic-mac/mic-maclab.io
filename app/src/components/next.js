import React, { useState } from "react"
import Agenda from "./agenda"
import Config from "./config"
import SearchContainer from "./SearchContainer"
import Contact from "./contact"
import Analytics from "./analytics"
import Dev from "./dev"
import Reporting from "./reporting"
import UxMissions from "./ux-missions"
import Slides from "./slides"
import GridSuggestions from "./grid-suggestions"
import MissionDisplay from "./mission-display"
import Seo from "./seo"
import SubmitMission from "./submit-mission"



export default props => {

    const components = {
        agenda: {
            jsx: <Agenda />
        },
        config: {
            jsx: <Config />
        },
        jsSearch: {
            jsx: <div style={{ color: `teal` }}>
                    <SearchContainer suggestions={props.suggestions} setVisibleDisplay={props.setVisibleDisplay} handleClickTuto={props.handleClickTuto}/>
                </div>
        }
        ,
        contact: {
            jsx: <Contact />
        },
        analytics: {
            jsx: <Analytics />
        },
        dev: {
            name: "dev",
            jsx: <Dev />
        },
        reporting: {
            jsx: <Reporting />
        },
        uxMissions: {
            jsx: <UxMissions />
        },
        slides: {
            jsx: <Slides />
        },
        gridSuggestions: {
            jsx: <GridSuggestions />
        },
        missionDisplay: {
            jsx: <MissionDisplay />
        },
        seo: {
            jsx: <Seo />
        },
        submitMission: {
            jsx: <SubmitMission />
        }
    }

    const [currentComponent, setCurrentComponent] = useState("agenda")

    const handleClick = (event) => {
        setCurrentComponent(event.target.value)
    }

    return (
        <div>
            <p>
               Click a button to display this next component 
            </p>
            <div>
                {
                    Object.keys(components).map(
                        key => <button value={key} onClick={handleClick}>{key}</button>
                    )
                }
            </div>
            <div>
                {components[currentComponent].jsx}
            </div>

        </div>
    )
}