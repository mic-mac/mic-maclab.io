import React from "react"

export default (props) => {
    
    const handleCrowdfundingClick = () => {
        window.open('https://opencollective.com/scenarioB', '_blank')
    }

    const raised = 68.53
    const goal = 500
    const actual = Math.floor(raised / goal * 100)

    return (
        <div style={{position: "fixed", bottom: "0px", left: "15%", right: "15%", width: "70%", backgroundColor: "white"}}>
                <div>
                    <div style={{textAlign: "right"}}>
                        <button  onClick={props.handleClickCross}>x</button>
                    </div>
                    <button style={{backgroundColor: "white", border: "0px", width: "100%"}} onClick={handleCrowdfundingClick}>
                        <div style={{paddingLeft: "10%", paddingRight: "10%", }}>
                            Soutenez le développement de cette dynamique par 1€/mois. {actual} % atteint ! Merci aux soutiens !
                        </div>
                        <div>
                            <progress value={actual} max="100" style={{height: "50px", width: "100%"}}>12%</progress>
                        </div>
                        <object type="image/svg+xml" data="https://opencollective.com/scenariob/tiers/contribuez-financierement-a-titre-individuel.svg?avatarHeight=26&width=1000&button=false">OpenCollective crowd</object>
                    </button>
                </div>
        </div>
    )
}