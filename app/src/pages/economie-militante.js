import React from "react"

export default () => {
    return (
        <div>
            
            <script src="https://static.airtable.com/js/embed/embed_snippet_v1.js"></script>
            <iframe title="airtable" class="airtable-embed airtable-dynamic-height"
            src="https://airtable.com/embed/shrs7XsWSVMBFHRNH?backgroundColor=purple" frameborder="0" onmousewheel="" width="100%" height="927.88333"
            style={{background:"transparent", border: "1px solid #ccc"}}></iframe>

            <h3 style={{textAlign:"center"}}>Le Mégaphone de l'économie militante fait partie de Scenario B</h3>
        
        </div>
    )
}