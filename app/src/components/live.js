import React, { useState } from "react"

export default (props) => {

    const [liveStatus, setLiveStatus] = useState("off")

    const handleLive = () => {
        setLiveStatus("on")
    }

    return(

        <div>
            {
                liveStatus === "off"
                ? <button onClick={handleLive}>Participer au live</button>
                :
                <iframe
                    allow="camera; microphone; display;"
                    src="https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true"
                    title="live"
                    width="100%"
                    height={props.current_display_infos.currentWindowHeight / 2.5}
                ></iframe>
            }
        </div>
    )
}