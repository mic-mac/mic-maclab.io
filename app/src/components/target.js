import React from "react"
import Formulairebeta from "./formulairebeta"

export default (props) => {

    const component_per_mission = [
        {
            "retranscrire-panneaux-de-chantiers": <Formulairebeta />,
            "referencer-panneaux-de-chantiers": <div>target not yet built</div>,
            "demarcher-maitre-oeuvre": <div>target not yet built</div>
        }
    ]

    return(
        <div>
            {props.mission}
            
            {component_per_mission[0][props.mission]}
        </div>
    )
}


/*

const mission = props.mission

    const [, setCurrentView] = useState("tuto")
    const [currentIframeUrl, setCurrentIframeUrl] = useState(props.missions[mission].tuto)

    const handleChangeView = (event) => {
        const newTarget = event.target.value
        setCurrentView(newTarget)
        setCurrentIframeUrl(props.missions[mission][newTarget])
    }






{
            props.showTarget === true ?            "Hé oh !" : "Pouet"}
            {
                props.showTarget === true
                ? null
                : <div>
                    <iframe
                        title="target"
                        width="100%"
                        height={props.current_display_infos.currentWindowHeight} //{props.current_display_infos.currentWindowHeight / 2.5}
                        src={currentIframeUrl}
                    ></iframe>
                    <br />
                    <button value="tuto" onClick={handleChangeView}>Tuto</button ><button value="zen" onClick={handleChangeView}>Zen</button><button value="chat" onClick={handleChangeView}>Chat</button>
                </div>
            }
            */