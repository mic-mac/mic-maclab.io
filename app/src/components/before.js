import React from "react"

export default (props) => {

    const recordLogin = () => {
        props.setLogin(true)
        window.localStorage.setItem("login", true)
    }

    const handleClickButton = (event) => {
        recordLogin()
        props.handleNavigationClicked(event)
        window.location = '/?filter=' + event.target.value
    }

    const login = () => {
        recordLogin()
        window.location.reload()
    }

    return (
        <div style={{paddingLeft: "30%", paddingRight: "30%"}}>
            
            <div style={{textAlign: "right"}}>
                <button style={{fontSize: "0.8em"}} onClick={login}>Connexion/Inscription</button>
            </div>
            <div style={{textAlign: "center"}}><h1>Scénario B</h1></div>
            <br />
            <div style={{backgroundColor: "lightgrey"}}>
                <p>
                    <strong>VOUS</strong> n'êtes pas seul·e à faire des choix engagés au quotidien ! 
                    <p >
                        <span style={{fontSize: "0.8em"}}>Voir les actions réalisées par les autres personnes et collectif motive à agir ! Vous aussi, partagez les actions concrètes que vous réalisez, même les plus petites !</span>
                        <div style={{textAlign: "center"}}><button value="reportages" onClick={handleClickButton}>Consultez les photo-reportages</button></div>
                    </p>
                </p>
            </div>
            <div style={{backgroundColor: "lightgrey"}}>
                <p>
                    Vous souhaitez contribuer à une action concrète ? 
                    <div style={{textAlign: "center"}}><button value="tutos" onClick={handleClickButton}>Consultez les guides d'action</button></div>
                </p>
            </div>
            <div style={{backgroundColor: "lightgrey"}}>
                <p>
                    Envie de passer à l'action sans attendre ?
                    <div>
                        <div style={{textAlign: "center"}}><button value="events" onClick={handleClickButton}>Consultez les dates des sessions d'action</button></div>
                    </div>
                </p>
            </div>

        </div>
    )
}