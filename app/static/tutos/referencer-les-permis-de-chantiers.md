Ce tuto est désormais stocké dans le json des missions

# Référencer les chantiers

## Pour sauver des matériaux qui peuvent être réemployés

----

### Chaque année…

- plusieurs chantiers de rénovation ont lieu dans votre quartier
- plusieurs tonnes de matériaux sont envoyés a la benne
- alors que ces matériaux pourraient être réemployés

alors…

----

### Aidez BOMA à sauver ces matériaux !

- les permis de construire sont en Open data après délai de 3 mois
- référencer les permis de construire plus tôt est un atout
- BOMA peut convaincre l'entreprise de démollition de sauver les matériaux de la benne
- et essayer de trouver un débouché aux matériaux !

----

### Votre mission si vous l'acceptez

Dès que vous voyez un panneau de permis de construire dans votre quartier,

<img width="20%" src="https://cdn.glitch.com/7422bd4d-ecf6-466e-b1f0-90873dfde531%2F_DSC0256.JPG?v=1601597211172"></img>
<img width="10%" src="https://cdn.glitch.com/7422bd4d-ecf6-466e-b1f0-90873dfde531%2F_DSC0262.JPG?v=1601597226063"></img>

Référencez-le chantiers sur [demolistras.gogocarto.fr/](https://demolistras.gogocarto.fr/) accessible en cliquant le bouton "Let's go" ci-dessous

<iframe width="90%" height="400px" src="https://demolistras.gogocarto.fr/"></iframe>

----

### Accédez au formulaire

En cliquant sur le bouton en bas à droite pour ouvrir le formulaire

![](https://cdn.glitch.com/7422bd4d-ecf6-466e-b1f0-90873dfde531%2FScreenshot%20from%202020-10-01%2013-20-56.png?v=1601551286421)

----

Le panneau de permis de construire ressemblera à ceci et vous retrouverez chaque ligne dans le formulaire

<img width="60%" src="https://cdn.glitch.com/7422bd4d-ecf6-466e-b1f0-90873dfde531%2F_DSC0262.JPG?v=1601597226063"></img>


----
À vous de jouer pour remplir le formulaire ci-dessous ! _Si vous n'avez pas le temps, entrez seulement l'adresse puis validez !_

![](https://cdn.glitch.com/7422bd4d-ecf6-466e-b1f0-90873dfde531%2FScreenshot%20from%202020-10-01%2013-23-03.png?v=1601551433472)


----

### Restons en contact

Demandez à vous inscrire à la liste d'information sur l'avancement

[contact@boma.alsace](mailto:contact@boma.alsace?subject=Tenez-moi%20au%20courant%20pour%20la%20mission%20de%20référencement%20des%20permis%20de%20construire)

Merci beaucoup de contribuer à la mission de BOMA en réalisant cette action !