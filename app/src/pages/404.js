import React from "react"

export default () => (
    <p>La page que vous recherchez n'est pas accessible. <a href="mailto:user701@orange.fr" >Dites-moi ce que vous cherchiez par email</a></p>
)