import React, { useState } from "react"
import Feed from "./feed"
import Navigation from "./navigation"
import Zen from "./zen"

import { StaticQuery, graphql } from 'gatsby'

export default (props, { data }) => {

  const extractMission = () => {
    const found = window.location.href.match(/[?&]mission=([^&]*)/)
    let mission
    if (found !== null) {
      if (found.length > 0) {
        mission = found[0].split("=")[1]
      }
    } else {
      mission = null
    }
    return mission
  }

  const [currentMission, setCurrentMission] = useState(extractMission())

  //var recommendations = missions

  const reportages = [
    {
      id: "123",
      text: "Text",
      link: "https://example.com",
      author: "Author",
      creationDate: "01/04/2021",
      chat: "https://example.com"
    },
  ]

  const events = [
    {
      id: "event01",
      text: "Event",
      link: "https://example.com",
      author: "Author",
      creationDate: "01/04/2021",
      chat: "https://example.com"
    },
  ]

  return (
    <div>
      <StaticQuery
        query={graphql`
            query MyQuery {
              allDataUrisCsv {
                edges {
                  node {
                    noteId
                    dataUri
                  }
                }
              }
            }
          `}
        render={data => {
          const tutos = data.allDataUrisCsv.edges.map(
            item => (
              {
                id: item.node.noteId,
                link: item.node.dataUri,
                text: "sample text",
                author: "Author",
                creationDate: "01/04/2021",
                chat: "https://example.com"
              }
            )
          )

          var allPosts = []

          const arrays = [
            { name: "reportages", array: reportages },
            { name: "tutos", array: tutos },
            { name: "events", array: events }
          ]

          for (var array of arrays) {
            const objtypé = []
            for (var item of array.array) {
              item["type"] = array.name
              objtypé.push(item)
            }
            allPosts = allPosts.concat(objtypé)
          }

          function shuffle(array) {
            var currentIndex = array.length, temporaryValue, randomIndex;

            // While there remain elements to shuffle...
            while (0 !== currentIndex) {

              // Pick a remaining element...
              randomIndex = Math.floor(Math.random() * currentIndex);
              currentIndex -= 1;

              // And swap it with the current element.
              temporaryValue = array[currentIndex];
              array[currentIndex] = array[randomIndex];
              array[randomIndex] = temporaryValue;
            }

            return array;
          }

          const posts = shuffle(allPosts)

          const postsIndex = {}

          for (var i = 0; i < posts.length; i++) {
            postsIndex[posts[i]["id"]] = i
          }

          console.log(posts)

          //----------------------

          return (

            <div>
              {
                currentMission !== null
                  ?
                  <div>
                    <Zen
                      currentMission={currentMission}
                      setCurrentMission={setCurrentMission}
                      posts={posts}
                      currentScreenSize={props.currentScreenSize}
                      postsIndex={postsIndex}
                    />
                    <h3>Recommendations</h3>
                  </div>
                  :
                  <Navigation
                    handleNavigationClicked={props.handleNavigationClicked}
                  />
              }
              <Feed
                posts={posts}
                currentScreenSize={props.currentScreenSize}
                //postsTypesToShow={props.postsTypesToShow}
                extractPostsTypesToShow={props.extractPostsTypesToShow}
                setCurrentMission={setCurrentMission}
              />
            </div>
          )
        }
        }
      />
    </div>
  )
}

/*


const tutosDictCache = {
            "referencement-panneaux-chantiers": {
              id: "referencement-panneaux-chantiers",
              text: "Référencement des panneaux de chantiers",
              mdSource: "",
              link: "https://pad.lamyne.org/p/referencement-panneaux-chantiers#/",
              target: "https://demolistras.gogocarto.fr",
              chat: "https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org",
              live: "https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true",
              author: "Author",
              creationDate: "01/04/2021",
            },
            "retranscrire-les-photos-de-panneaux-de-chantiers": {
              id: "retranscrire-les-photos-de-panneaux-de-chantiers",
              text: "Référencement des panneaux de chantiers",
              mdSource: "",
              link: "https://pad.lamyne.org/p/retranscrire-les-photos-de-panneaux-de-chantiers#/",
              target: "https://demolistras.gogocarto.fr",
              chat: "https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org",
              live: "https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true",
              author: "Author",
              creationDate: "01/04/2021",
            },
            "contacter-un-maitre-d-oeuvre": {
              id: "contacter-un-maitre-d-oeuvre",
              text: "Référencement des panneaux de chantiers",
              mdSource: "",
              link: "https://pad.lamyne.org/p/contacter-un-maitre-d-oeuvre#/",
              target: "https://demolistras.gogocarto.fr",
              chat: "https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org",
              live: "https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true",
              author: "Author",
              creationDate: "01/04/2021",
            },
            "no-mission-selected": {
              id: "no-mission-selected",
              text: "no-mission-selected",
              link: "https://no-mission-selected",
              target: "https://no-mission-selected",
              chat: "https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org",
              live: "https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true",
              author: "Author",
              creationDate: "01/04/2021",
            },
            "demarchage-friches-municipalites": {
              id: "demarchage-friches-municipalites",
              text: "Démarchage des friches des municipalités",
              tuto: "https://nbandassi.files.wordpress.com/2021/02/guide-prospection-mairies-1.pdf",
              target: "https://cartemata.gogocarto.fr/annuaire#/carte/strasbourg@46.48,2.38,6z?cat=all@+f6",
              live: "https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true",
              chat: "https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org",
              author: "Author",
              creationDate: "01/04/2021",
            },
            "posts-de-bienvenue-ecocolocations": {
              id: "posts-de-bienvenue-ecocolocations",
              text: "Écrire un post de bienvenue",
              tuto: "https://eco-colocations.org/contribution/posts-de-bienvenue",
              target: "https://eco-colocations.org/contribution/posts-de-bienvenue",
              chat: "https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org",
              live: "https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true",
              author: "Author",
              creationDate: "01/04/2021",
            },
            "partage-photo-reportages-initiatives-collectives": {
              id: "partage-photo-reportages-initiatives-collectives",
              text: "Partager les photo-reportages d'initiatives collectives",
              tuto: "https://example.com",
              target: "https://feed.eugenemolotov.ru/?action=display&bridge=Facebook&context=Group&g=https%3A%2F%2Fwww.facebook.com%2Fgroups%2F2171571459596257&limit=-1&format=Html",
              chat: "https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org",
              live: "https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true",
              author: "Author",
              creationDate: "01/04/2021",
            },
            "suivi-posts-ecocolocations": {
              id: "partage-photo-reportages-initiatives-collectives",
              text: "Partager les photo-reportages d'initiatives collectives",
              tuto: "https://example.com",
              target: "https://feed.eugenemolotov.ru/?action=display&bridge=Facebook&context=Group&g=https%3A%2F%2Fwww.facebook.com%2Fgroups%2F2171571459596257&limit=-1&format=Html",
              chat: "https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org",
              live: "https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true",
              author: "Author",
              creationDate: "01/04/2021",
            },
            "suivi-etat-resolution-posts-ecocolocations": {
              id: "suivi-etat-resolution-posts-ecocolocations",
              text: "Suivre l'état de résolution des posts éco-colocations",
              tuto: "https://eco-colocations.org/contribution/resolution",
              target: "https://example.com",
              chat: "https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org",
              live: "https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true",
              author: "Author",
              creationDate: "01/04/2021",
            },
            "fact-checker-conseil-local": {
              id: "fact-checker-conseil-local",
              text: "Fact-checker un conseil local",
              tuto: "https://example.com",
              target: "https://mic-mac.gitlab.io/zen/fact-checker-conseil-local",
              chat: "https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org",
              live: "https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true",
              author: "Author",
              creationDate: "01/04/2021",
            }
          }

  */