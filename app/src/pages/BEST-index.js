import React from "react"
import App from "../components/app"
import Chat from "../components/chat"
import Cloud from "../components/cloud"
import Loading from "../components/loading"

export default () => {

    const location = typeof window !== 'undefined' ? window.location.href : null

    const locations = [
        {
            url:"https*://.*\\.gitpod\\.io/*.*",
            jsx: <App />
        },
        {
            url:"https*://localhost/*.*",
            jsx: <App />
        },
        {
            url:"https*://mic-mac.gitlab\\.io/*.*",
            jsx: <App />
        },
        {
            url: "https*://(www\\.)*scenariob\\.org/*.*",
            jsx: <App />
        },
        {
            url: "https*://(www\\.)*agirpourun\\.scenariob\\.org/*.*",
            jsx: <App />
        },
        {
            url: "https*://(www\\.)*cloud\\.scenariob\\.org/*.*",
            jsx: <Cloud />
        },
        {
            url:"https*://(www\\.)*chat\\.scenariob\\.org/*.*",
            jsx: <Chat />
        },
    ]

    return (
        <div>
            {
                typeof window !== 'undefined'
                ?
                    locations.map( item => 
                        <div>
                        { 
                            location.search(item.url) !== -1
                            ? item.jsx
                            : null
                        }
                        </div>
                    )
                :
                    <Loading />
                
            }
        </div>
    )
    
}