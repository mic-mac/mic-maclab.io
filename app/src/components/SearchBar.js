import React from "react"
import ComponentLayout from "./component-layout"
import SearchContainer from "./SearchContainer"

export default props => {

    const padding = props.position  === "middle" ? "15%" : "2%"

/*    var queryInApp = props.queryInApp

     const handleKeyUp = (event) => {
        props.handleKeyUp(event, queryInApp)
    } */

    return (
        <div>
            <ComponentLayout displayComponentsDebug={props.displayComponentsDebug} setDisplayComponentsDebug={props.setDisplayComponentsDebug}>


                <div>
                    <div style={{ paddingTop: padding, textAlign: "center" }}>
                        <SearchContainer suggestions={props.suggestions} setVisibleDisplay={props.setVisibleDisplay} handleClickTuto={props.handleClickTuto}/>

                    </div>

{/*                     <div>
                        { ( props.displaySearchSuggestionsMenu === true && props.queryInApp.length > 2 )  ? (
                        <div style={{ textAlign: "center"}}>
                            {
                                Object.keys(props.suggestions.results).map(
                                    item => <div>
                                        <button
                                            value={props.suggestions.results[item].id}
                                            onClick={props.handleSuggestionClick}
                                            style={{width: "60%"}}
                                        >
                                            {props.suggestions.results[item].fields.Titre}
                                        </button>
                                        <br />
                                    </div>
                                )
                            }
                        </div>) : null} 
                    </div>*/}


                </div> 
                
            </ComponentLayout>
        </div>
    )
}

