from subprocess import run, check_output
from os import environ
from re import sub
from base64 import b64encode

def backup_former_guides():
    run(["cp", "slides/raw/*", "slides/backup/."])

def get_remote_guide_markdown(source_url):
    url_parts = source_url.split("/")
    host = "/".join(url_parts[0:3])
    note_id = url_parts[3].split("#")[0]

    environ["HEDGEDOC_SERVER"] = host
    run(["hedgedoc", "export", "--md", note_id, "slides/raw/{}.md".format(note_id)])
    run(["touch",  "slides/backup/{}.md".format(note_id)]) # crée un backup pour comparer (à quelque chose de différent)

    return note_id

def check_if_guide_changed(note_id):
    try:
        diff = check_output(["diff", "slides/raw/{}.md".format(note_id), "slides/backup/{}.md".format(note_id)])
        if diff == b"":
             should_be_parsed = False
        else:
            should_be_parsed = True
    except:
        should_be_parsed = True
    
    return should_be_parsed

def parse_raw_markdown(note_id):
    md_content = open("slides/raw/{}.md".format(note_id), 'r').read()
    content_split = md_content.split("---")
    metadata = {}

    for meta in content_split[1].split("\n")[1:-1]:
        split = meta.split(":")
        metadata[split[0]] = ":".join(split[1:])

    slides = "---".join(content_split[2:])

    computed_content = {}

    for key in metadata.keys():
        computed_content[key] = """<iframe src="{}" width="100%" height="600px">
</iframe>""".format(metadata[key])

    full_markdown = "{}\n\n---\n\n{}\n\n---\n\n{}\n".format(slides, 0, 1) #, computed_content["target"], computed_content["result"])


    template = open("slides/template.html", 'r').read()
    template_filled = sub("{{{SLIDES_markdown_encoded}}}", full_markdown, template)
    template_filled_encoded = b64encode(bytes(template_filled, encoding="UTF-8")).decode()

    interm_content = "data:text/html;base64,{}".format(template_filled_encoded)

    return interm_content

def record_data_uri(note_id, interm_content):
    open("app/src/data/DataUris.csv", 'a').write('"{}","{}"\n'.format(note_id,interm_content))

#def record_parsed_markdown(interm_content, note_id):
#    open("slides/interm/{}.md".format(note_id), 'w').write(interm_content)
#    return 0

#def update_remote_pad(note_id):
#     environ["HEDGEDOC_SERVER"] = "https://pad.lamyne.org"
#     run(["hedgedoc", "login", "--email", HEDGEDOC_EMAIL, HEDGEDOC_PASSWORD])
#     new_note_byte = check_output(["hedgedoc", "import", "slides/interm/{}.md".format(note_id)]) # , note_id (to update the note)
#     new_note = str(new_note_byte).split("\\n")[1]
#     return new_note

#def record_guide_url_for_note(note_id, new_note):
#    with open("slides/guides_adresses.csv", 'a') as f:
#        f.write("{},{}\n".format(note_id, new_note))

if __name__ == "__main__":

    if ("HEDGEDOC_EMAIL" not in environ) | ("HEDGEDOC_PASSWORD" not in environ):
        HEDGEDOC_EMAIL = input("pad.lamyne.org HEDGEDOC_EMAIL: ")
        HEDGEDOC_PASSWORD = input("pad.lamyne.org HEDGEDOC_PASSWORD: ")

    backup_former_guides()

    with open("slides/sources.csv", 'r') as f:
        sources_urls = f.read()[:-1].split("\n")

    for source_url in sources_urls:
        note_id = get_remote_guide_markdown(source_url)

        if check_if_guide_changed(note_id):
             interm_content = parse_raw_markdown(note_id)
             record_data_uri(note_id, interm_content)
             # record_parsed_markdown(interm_content, note_id)
             # new_note = update_remote_pad(note_id)
             # record_guide_url_for_note(note_id, new_note)

