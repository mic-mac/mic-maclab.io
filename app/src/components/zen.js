import React from "react"
import DashboardTop from "./dashboardTop"
import DashboardDown from "./DashboardDown"

export default (props) => {
    const handleBackClicked = () => {
        //window.history.back() // TODO: make the former page stable
        window.location = "/"
        //props.setCurrentMission(null)
    }

    return (
        <div>
            <div style={{textAlign: "left"}}><button onClick={handleBackClicked}>Back</button></div>
            
            <DashboardTop
                currentScreenSize={props.currentScreenSize}
                posts={props.posts}
                postsIndex={props.postsIndex}
                currentMission={props.currentMission}
            />

            <DashboardDown
                currentScreenSize={props.currentScreenSize}
                posts={props.posts}
                postsIndex={props.postsIndex}
                currentMission={props.currentMission}
            />

            
            <div style={{textAlign: "left", paddingLeft: "10%", paddingRight: "10%"}}>
                <span style={{paddingRight: "1%"}}><button>J'aime</button></span>
                <span style={{paddingRight: "1%"}}><button>Je n'y arrive pas</button></span>
            </div>
        </div>
    )
}