import React from "react"

export default props => (
    <div>
        <script src="https://static.airtable.com/js/embed/embed_snippet_v1.js"></script>
        <iframe title="airtable" class="airtable-embed airtable-dynamic-height" src={props.airtableUrl} frameborder="0" onmousewheel="" width="100%" height="533"
        style={{background: "transparent"}}></iframe>
    </div>
)