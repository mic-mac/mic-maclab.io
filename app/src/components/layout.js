import React from "react"
import "../styles/app-layout.css"


export default props => {
    return (
        <div style={{
            backgroundColor: "#efeff5"
        }}>
            {props.children}
        </div>
    )
}