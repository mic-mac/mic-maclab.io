import React from "react"

export default (props) => {
    return(
        <div style={{textAlign: "left"}}>
            mobile
            <iframe 
                title="link"
                width="95%"
                height={props.currentScreenSize.width}
                src={props.posts[props.postsIndex[props.currentMission]]["link"] + props.currentView}
                allow="camera; microphone; display;"
            ></iframe>
        </div>
    )
}