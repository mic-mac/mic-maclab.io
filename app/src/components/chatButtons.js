import React from "react"

export default (props) => {

    const handleButtonClicked = (event) => {
        props.setCurrentView(event.target.value)
    }

    return(
        <div>
            {
                [{text: "Chat", value: "chat"}, {text: "Live", value: "live"}].map(
                    item => 
                        <span style={{paddingRight: "1%"}}><button value={item.value} onClick={handleButtonClicked} style={{height: "50px", }}>{item.text}</button></span>
                )
            }
            
        </div>
    )
}