import React from "react"
import LayoutAnalytics from "./layout-analytics"

const Analytics = () => {return(
    <div>
        <LayoutAnalytics>
            <h2>Analytics</h2>
            <a href="https://gitlab.com/mic-mac/simul">Repo mic-mac/simul</a>
        </LayoutAnalytics>
    </div>
)}

export default Analytics