import i18n from 'i18next';   
import { initReactI18next } from 'react-i18next';  

i18n  
  .use(initReactI18next)  
  .init({
    debug: true, // pratique pour voir les événements dans la console pendant le développement
    lng: 'en',
    fallbackLng: 'en',
    resources: {
      fr: {
        translation: { // namespace par défaut, on peut avoir autant de namespaces que l'on souhaite
          "Prendre le pouls": "Prendre le pouls",
          "Inspirations": "Inspirations",
          "Missions": "Missions",
          "Un truc cloche": 'Un truc cloche',
          "Reporting": "Reporting",
          "Rechercher": "Rechercher"
        },
      },
      en: {
        translation: {
          "Prendre le pouls": "Pulse",
          "Inspirations": "Inspirations",
          "Missions": "Missions",
          "Un truc cloche": 'Something is wrong',
          "Reporting": "Reporting",
          "Rechercher": "Search"
        },
      },
    },
  });
  
export default i18n;
