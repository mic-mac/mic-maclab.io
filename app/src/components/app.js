import React, { useState, useLayoutEffect } from "react"
import Activity from "./activity" 
import Before from "./before" 
import Crowdfunding from "./crowdfunding"

export default () => {

    const originalLogin = typeof window != 'undefined' ? window.localStorage.getItem("login") : null
    const [login, setLogin] = useState(originalLogin)

    const location = typeof window !== 'undefined' ? window.location.href : null

    //--------------
    // Window size
    function useWindowSize() {
        const [size, setSize] = useState([0, 0]);
        useLayoutEffect(() => {
            function updateSize() {
            setSize([window.innerWidth, window.innerHeight]);
            }
            window.addEventListener('resize', updateSize);
            updateSize();
            return () => window.removeEventListener('resize', updateSize);
        }, []);
        return size;
    }
    
    function ShowWindowDimensions(props) {
        const [width, height] = useWindowSize()
        return {width: width, height:height}
    }

    const currentScreenSize = ShowWindowDimensions()

    //---------------

    const allPostsTypes = ["reportages", "tutos", "events"]

    // understands if parameters are passed through the URI. Later changes are handled through state
    const extractPostsTypesToShow = () => {
        const found = window.location.href.match(/[?&]filter=([^&]*)/)
        let filter
        if (found !== null){
            if (found.length > 0) {
                filter = found[0].split("=")[1].split(",")
            }
        } else {
            filter = allPostsTypes
        }
        return filter
    }
    
    // App feeling vs browser feeling (cf "copy link" button)
    const [postsTypesToShow, setPostsTypesToShow] = useState(extractPostsTypesToShow())

    const handleNavigationClicked = (event) => {
        const value = event.target.value
        if (value === "none"){  
            setPostsTypesToShow(allPostsTypes)
            //window.location = "/"
        }
        else{
            setPostsTypesToShow([value])
            //window.location = "/?filter=" + [value].join(",")
        }
    }

    //---------------

    const sidePadding = currentScreenSize.width > 500 ? "20%" : "0"

        
    //const [currentPage, setCurrentPage] = useState("index")

    //const [showTarget, setShowTarget] = useState(true)

    const initialHideCrowdfunding = typeof window === 'undefined' ? window.localStorage.getItem("hideCrowdfunding") : null
    const [hideCrowdfunding, setHideCrowdfunding] = useState(initialHideCrowdfunding)

    const handleClickCross = () => {
        window.localStorage.setItem("hideCrowdfunding", true)
        setHideCrowdfunding(true) // only managed through localStorage
    }

    return(
        <div style={{textAlign:"center", paddingRight:sidePadding, paddingLeft:sidePadding}}>
            {hideCrowdfunding}
            {
                login === null
                ?
                <Before 
                    handleNavigationClicked={handleNavigationClicked}
                    setLogin={setLogin}
                />
                :
                    <div>
                        <Activity
                            location={location}
                            currentScreenSize={currentScreenSize}
                            postsTypesToShow={postsTypesToShow}
                            extractPostsTypesToShow={extractPostsTypesToShow}
                            handleNavigationClicked={handleNavigationClicked}
                        />
                    </div>
            }

                    <div>
                    {
                        hideCrowdfunding === true | window.localStorage.getItem("hideCrowdfunding") === "true"
                        ?
                            null
                        : 
                            <div>
                                <Crowdfunding
                                    handleClickCross={handleClickCross}
                                />
                            </div>
                    }
                    </div>
            
        </div>
    )
}