import React, { useState } from "react"
import ZenButtons from "./zenButtons"
import MobileView from "./mobileView"
import DesktopView from "./desktopView"

export default (props) => {

    const [currentView, setCurrentView] = useState("target")

    return(
        <div>
                
                {
                    props.currentScreenSize.width < 500
                    ?
                        <MobileView
                            currentScreenSize={props.currentScreenSize}
                            posts={props.posts}
                            postsIndex={props.postsIndex}
                            currentMission={props.currentMission}
                            currentView={currentView}
                        />
                    :
                        <DesktopView
                        currentScreenSize={props.currentScreenSize}
                            posts={props.posts}
                            postsIndex={props.postsIndex}
                            currentMission={props.currentMission}
                            currentView={currentView}
                        />
                }
                <div style={{display: "table", height: "20%", overflow: "hidden"}}><div style={{position:"absolute", right: "1%", top: "25%", height:"10%", verticalAlign:"50%", backgroundColor: "lightgrey",display: "tableCell"}}>></div></div>
                
                <ZenButtons
                    setCurrentView={setCurrentView}
                />
            </div>
    )
}