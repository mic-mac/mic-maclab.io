function subscribeUser () {
    // Return subscription request promise to caller
    console.log("Subscribe user " + swSub)
    return swReg.pushManager.subscribe(
        {
        userVisibleOnly: true,
        applicationServerKey: applicationServerKey
        }
      )
  }
  function unsubscribeUser () {
    // Return promise to calling function
    console.log ("Unsubscribe user " + swSub)
    return swSub.unsubscribe()
    }
  // Service routine to contact our speciic server
  function sendSubscriptionToBackEnd(subscription) {
    // Convert the subscription to a simple object
    let bodyObject = subscription.toJSON()
    // This will eventually be sent to a notification microservice
    return fetch(<INSERT URL OF PUSH SERVER HERE>, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(bodyObject)
    })
    .then(function(response) {
      if (!response.ok) {
        throw new Error('Bad status code from server.');
      }
      // console.log('Subscribe response: ' + JSON.stringify(reponse.data))
      return response.json();
    })
    .then(function(responseData) {
      if (!(responseData.data && responseData.data.success)) {
        throw new Error('Bad response from server.');
      }
      console.log(JSON.stringify(responseData.data))
    });
  }

serviceWorkerRegistration.showNotification("Test", 
{
    "body": "Did you make a $1,000,000 purchase at Dr. Evil...",
    "icon": "images/ccard.png",
    "vibrate": [200, 100, 200, 100, 200, 100, 400],
    "tag": "request",
    "actions": [
      { "action": "yes", "title": "Yes", "icon": "images/yes.png" },
      { "action": "no", "title": "No", "icon": "images/no.png" }
    ]
  }
);

setTimeout(() => {
  self.registration.showNotification("Hello, world!")
}, 15000)