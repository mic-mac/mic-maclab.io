/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: `Mic Mac`,
    description: `Mic mac, une application pour contribuer aux projets qui vous veulent du bien`,
    author: `jibe-b`,
  },
  plugins: [
    `gatsby-plugin-emotion`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Mic-mac`,
        short_name: `micmac`,
        start_url: `/`,
        background_color: `#6b37bf`,
        theme_color: `#6b37bf`,
        display: `standalone`,
        icon: `src/images/icon.png`,
      },
    },
    {
      resolve: `gatsby-plugin-offline`,
        options: {
          importScripts: [
            `./sw-extension.js`
          ],
          cacheId: `micmac`
        }
      },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `src`,
        path: `${__dirname}/src/`,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/src/data/`,
      },
    },
    `gatsby-transformer-csv`,
    /*`gatsby-transformer-json`,*/
    /*`gatsby-transformer-remark`,*/
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    /*'gatsby-theme-mdx-deck',*/
/*     {
      resolve: `gatsby-plugin-i18n`,
      options: {
          langKeyDefault: 'en',
          langKeyForNull: 'en',
          prefixDefault: false,
          useLangKeyLayout: false,
      },
    }, */
    /* {
      resolve: `gatsby-source-airtable`,
      options: {
        apiKey: process.env.AIRTABLE_API_KEY, // may instead specify via env, see below
        concurrency: 5, // default, see using markdown and attachments for more information
        tables: [
          {
            baseId: `appuSkMutNImas9bL`,
            tableName: `Y a un truc qui cloche`,
          },
        ]
      }
    } */
  ],
}
