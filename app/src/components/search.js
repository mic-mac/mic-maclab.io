import React from "react"
import SearchBar from "./search-bar"
import Incitation from "../components/Incitation"

export default props => {

  return (
    <div>
      <div style={{textAlign: "center"}}>

        <div style={{paddingTop: "20%", paddingBottom: "30%"}}>

          <Incitation />
          <SearchBar />
        </div>
      </div>


    </div>
  )
}