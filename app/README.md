# Mic-Mac - appli pour outiller la dynamique ScénarioB

## Installer l'instance officielle

Consulter https://agirpourun.scenarioB.org et cliquer sur le bouton "Installer l'application" fourni par votre navigateur

## Servir localement

Prérequis : node10

>npm install
>gatsby develop

## Projet pédagogique

Projet pédagogique des coopétitions ScénarioB

Note préliminaire : ScénarioB exige que la gouvernance soit dans les mains de celles et ceux qui doivent vivre avec les conséquences des décisions prises, conjointement avec celles et ceux qui ont les savoirs et des expériences sur les sujets en question

Une liste d'éléments de design d'un projet ou de démarche individuelle viennent rendre cette gouvernance partagée optimale :

- Autogestion : rotation des rôles et de la responsabilité de l'arbitrage des prises de décision
- Étudiabilité et reproductibilité : description détaillée de l'ensemble du cheminement & documentation plug&play
- Evience-based&peer-review (communication asynchrone) : chaque processus de prise de décision commence par une phase d'instruction contradictoire et sourcée. L'instruction ouvre toute issue envisagée, l'instruit et la pondère/priorise
- design de résolution de problèmes : à partir de l'instruction préalable du dossier, proposer différents designs et les expérimenter au plus vite en les confrontant à la réalité / acceptabilité
- apprentissage permanent, amélioration des process, élaguage dès que possible

## Déroulement des coopétitions ScénarioB

## Captation vidéo

- des séances collectives sont organisées
- chaque équipe est invitée à remonter des capsules vidéos prises au cours de leur réalisation du défi
- ces éléments vidéos sont montés (en impliquant la communauté) et diffusés chaque soir

### Design d'un défi

- Chaque participant·e peut participer à designer un défi (voir la section "comment participer")
- les propositions de défis doivent respecter la liste de critères des défis (voir section "critères des défis")
- des consultations sont réalisées régulièrement pour choisir le défi suivant

### Phase de compétition

- les équipes constituées sont invitées à se pencher sur le défi
- des pistes, des indications et des moyens sont données à chacune des équipes
- pendant la phase de compétition, les équipes tâchent de résouvre le défi sur la zone qui leur est attribuée, en veillant à ne pas échanger avec les autres équipes
- chaque équipe veille à bien instruire son sujet avec des données
- chaque équipe envoie son état actuel pour qu'il soit évalué/confronté à la réalité afin d'avoir une indication de l'avancement de sa proposition de résolution
- chaque équipe documente au mieux
- différents critères amènent à réaliser des actions secondaires
- cette phase est limitée dans le temps

### Phase de coopération

- à l'issue de la phase de compétition, la documentation et les évaluations de l'avancement de chaque projet sont mises à disposition de tou·te·s pour permettre de s'inspirer des éléments qui ont marché et identifier ce qui semble empêcher de réussir
- l'objectif au cours de la phase de coopération est d'améliorer au mieux la méthode et de collectivement faire monter la jauge globale

### Maintien des dynamiques mises en place

- les dynamiques mises en place ont pour vocations à être maintenues dans le temps
- un espace est disponible pour poursuivre l'animation de ces dynamiques
- une cagnotte à laquelle abondent des individus et des organisations permet de rétribuer les contributions qui dépassent les coups de mains gratuits
- en particulier des missions d'animation de la dynamique
- l'acceptation des candidatures est réalisée par les sociétaires de la SCIC ScénarioB

### Comment participer ?

- pour tou·te·s : participez aux sessions d'entraînement concret, régulières et ouvertes à tou·te·s
- participez à un défi : prenez une part sociale dans la SCIC ScénarioB, et c'est parti !

### Comment est-ce financé ?

La cagnotte remplie par financement participatif abondé par des individus et des coopératives et associations (et
sponsors si et seulement si contrat pour expliciter l'indépendance)

## Critères des défis

## Critères d'évaluation des solutions proposées

- auto-entretien : à quel point le design peut se poursuivre simplement à partir de la méthode disponible en ligne et la dynamique amorcée
- résilience : à quel point la dynamique se poursuit même si des parties de la structure tombent
- newcomers-friendly : à quel point une personne qui vient d'arriver peut s'impliquer au moins à un niveau (avec une perspective de progresser)

## Premiers défis

### Ta Commande

#### Objectif

Dégager des budgets pour les initiatives citoyennes de solidarité

#### Périodes

- compétition : novembre
- coopération : décembre

(pour démarrer 2021 sur les chapeaux de roue)

#### Moyens

Groupe facebook Ta Commande et les espaces que vous souhaiterez

#### Défi

Développer dans votre quartier des groupes d'achat en commun pour négocier les prix et dégager des économies pour fournir des budgets aux actions de solidarité

#### Indications

- collecter des données de consommation et d'intentions d'achat
- expliciter une commande
- la proposer aux potentiel·le·s participant·e·s
- documenter chaque étape
- prendre contact avec des fournisseurs/producteurs
- négocier des prix
- échanger avec les assos réalisant des actions de solidarité
- mobiliser du monde pour participer à Ta Commande

#### Compétition

Designer, mettre en route un groupement d'achat dans votre quartier

#### Coopération

- optimiser la/les designs de groupements d'achat
- les implémenter dans un maximum d'endroit (possibilité de se joindre à la démarche ?)
- embarquer des consommateur·ices
- réaliser des commandes
- faire monter la barre de dégagement de fonds pour les assos de maraude

#### Poursuite de la dynamique

- fiches missions
- marche-à-suivre
- animation pour accompagner les gens qui se lancent

### Mégaphone des Initiatives Citoyennes

#### Objectif

Rendre visibles les actions concrètes des initiatives citoyennes et amener à les suivre sur le long terme

#### Périodes

- compétition : décembre
- coopération : janvier

(pour commencer l'année 2021 sur les chapeaux de roues)

#### Moyens

Groupe facebook Mégaphone, Transiscope

#### Défi

- amener des acteurs à réaliser des reportages sur leurs actions concrètes
- amener des citoyen·ne·s à suivre ces reportages
- amener à répondre à des sondages
- amener à contribuer à rendre visible les pages fb des assos

#### Indications

- chercher les actions citoyennes actives et compléter les annuaires
- rechercher les reportages déjà réalisés

#### Compétition

- réaliser ce défi pour sa ville/quartier

#### Coopération

- faire connaître les initiatives citoyennes d'une ville aux gens des autres villes

#### Poursuite de la dynamique

- animation du Mégaphone
- relance des assos
- sondage auprès des citoyen·ne·s
- animer des sessions de contribution


### Exode réseaux sociaux

#### Périodes

- compétition : 
- coopération : 

#### Moyens

#### Défi

#### Indications

#### Compétition

#### Coopération

#### Poursuite de la dynamique


### Atelier contributif de fabrication de meubles

#### Périodes

- compétition : 
- coopération : 

#### Moyens

#### Défi

#### Indications

#### Compétition

#### Coopération

#### Poursuite de la dynamique




### Une personne par rue pour tenir à jour la carte des permis de chantier

#### Périodes

- compétition : Février
- coopération : Mars

#### Moyens

La carto Demolistras & le groupe Les Bonnes Matières (un post pour chaque panneau de chantier)

#### Défi

- Motiver et briefer une personne qui passe par chaque rue pour être les yeux qui repèrent les panneaux de permis de chantier. Objectif : une point "yeux" par rue
- Tenir à jour un maximum de panneaux de chantier (une autre équipe pourrait passer voir s'il y en a qui manquent/pas à jour)
- Drôlerie : racontez une anecdote drôle qui s'est passée dans cette rue

#### Indications

- demandez à vos voisins
- parlez du réemploi de matériaux du bâtiment (second œuvre)
- parlez avec les gens qui ont rénové récemment/vont rénover
- allez dans la rue demander des anecdotes

#### Compétition

- motiver du monde pour les rues d'un quartier. Pendant un temps court
- enregistrer le récit des anecdotes

#### Coopération

- sur plusieurs mois,
- étendre petit à petit à d'autres quartiers
- amener la jauge des panneaux à être la plus haute possible

#### Poursuite de la dynamique

- animation dans le groupe Les Bonnes Matières (intéressé·e·s par le réemploi)
- et dans le groupe "Urbanisme participatif" (cartographie - tous sujets)