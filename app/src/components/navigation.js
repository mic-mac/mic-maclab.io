import React from "react"

export default (props) => {
    return(
        <div>
            <button value="none" onClick={props.handleNavigationClicked}>Home</button>
            <button value="reportages" onClick={props.handleNavigationClicked}>Reportages</button>
            <button value="tutos" onClick={props.handleNavigationClicked}>Guides</button>
            <button value="events" onClick={props.handleNavigationClicked}>Events</button>
            <button>Search</button>
        </div>
        )
}