import React, { useLayoutEffect, useState } from "react"
import Axios from "axios"
import "../styles/dropdown-menu-style.css"
import Navigation from './navigation'

export default (props) => {

    const currentURL = typeof window !== undefined ? window.location.href : null
    var backup_tuto_url = ""
    if (typeof window !== undefined){
        if (currentURL.search("https*://[^/]*\.gitpod.io") !== -1){
            backup_tuto_url = currentURL.replace("8000", "9000")
        }
    }


    // localStorage / json-server
    const currentProjectInLocalStorageAtStart = typeof window !== 'undefined' ? localStorage.getItem("currentProject") : null
    const initialProject = currentProjectInLocalStorageAtStart === null ? "templateMission" : currentProjectInLocalStorageAtStart

    const [currentProject, setCurrentProject] = useState(initialProject)
    
    //changeUrl("currentProject", currentProject, "add")

    // localStorage / json-server
    //à la place : modifier window.location
    const indexOrLiveInLocalStorageAtStart = typeof window !== 'undefined' ? localStorage.getItem("indexOrLive") : null
    const initialIndexOrLive = indexOrLiveInLocalStorageAtStart === null ? "index" : indexOrLiveInLocalStorageAtStart
    
    const [indexOrLive, setindexOrLive] = useState(initialIndexOrLive)

    const handleIndexOrLive = (event) => {
        const newIndexOrLive = "live"
        setindexOrLive(newIndexOrLive)
        if (typeof window !== 'undefined'){
            localStorage.setItem("indexOrLive", newIndexOrLive)
        }

        const newShowWhat = "tuto"
        setShowWhat(newShowWhat)
        if (typeof window !== 'undefined'){
            localStorage.setItem("showWhat", newShowWhat)
        }

        const newCurrentProject = event.target.value
        setCurrentProject(newCurrentProject)
        if (typeof window !== 'undefined'){
            localStorage.setItem("currentProject", newCurrentProject)
        }

        window.scrollTo(0, 0);
    }

    const handleGoIndex = () => {
        const newIndexOrLive = "index"
        setCurrentProject("index")
        setindexOrLive(newIndexOrLive)
        // if (typeof window !== 'undefined'){
            localStorage.setItem("indexOrLive", newIndexOrLive)
        //}
    }

    const [onAir, setOnAir] = useState("off") // json-server
    /*
    const handleOnAir = () => {
        const newOnAir = onAir === "off" ? "on" : "off"
        setOnAir(newOnAir)
        alert("passer par json-server désormais")
    }
    */    

    //51.178.183.220 --port 3000
    //json-server --watch --host 127.0.0.1 --port 3000 app/../json-server/data.json
    //Axios.get("http://127.0.0.1:3000/visios",
    Axios.get("https://api.scenariob.org/visios")
      .then(result => {
          setOnAir(result.data["onair"])
      })
      .catch(err => {
        //this.setState({ isError: true })
            console.log(`====================================\nSomething bad happened while fetching the data\n${err}\n====================================`)

      })

    const showWhatInLocalStorageAtStart = null // localStorage.getItem("showWhat")

    // localStorage / json-server
    const initialShowWhat = showWhatInLocalStorageAtStart === null ? "tuto" : showWhatInLocalStorageAtStart
    const [showWhat, setShowWhat] = useState(initialShowWhat) 
    const handleShowWhat = () => {
        const newShowWhat = showWhat === "tuto" ? "zen" : "tuto"
        setShowWhat(newShowWhat)
        if (typeof window !== 'undefined'){
            localStorage.setItem("showWhat", newShowWhat)
        }
        //à la place : modifier window.location
    }

    const [showHelpLaunchInBrowser, setShowHelpLaunchInBrowser] = useState(true)
    const hideMe = () => {
        setShowHelpLaunchInBrowser(false)
    }

    function useWindowSize() {
        const [size, setSize] = useState([0, 0]);
        useLayoutEffect(() => {
            function updateSize() {
            setSize([window.innerWidth, window.innerHeight]);
            }
            window.addEventListener('resize', updateSize);
            updateSize();
            return () => window.removeEventListener('resize', updateSize);
        }, []);
        return size;
    }

    //--------------
    function ShowWindowDimensions(props) {
        const [width, height] = useWindowSize()
        return {width: width, height:height}
    }

    const currentSize = ShowWindowDimensions()
    const currentWindowWidth = currentSize.width
    const currentWindowHeight = currentSize.height
    //---------------

    const [shouldIGetRemoteZenData, setShouldIGetRemoteZenData] = useState(true)
    //json-server --watch --host 127.0.0.1 --port 3000 app/../json-server/data.json
    if (shouldIGetRemoteZenData === true){
        //Axios.get("http://127.0.0.1:3000/missions",
        Axios.get("https://api.scenariob.org/missions",
            {
                headers: {
                    'Access-Control-Allow-Origin': '*',
                }
            
            })
        .then(result => {
            const newZenData = result.data
            updateZenDataWithRemoteData(newZenData)
            setShouldIGetRemoteZenData(false)
        })
        .catch(err => {
            console.log(`====================================\nSomething bad happened while fetching the data\n${err}\n====================================`)
        })
    }

    const handleClickEdit = () => {
        alert("not yet implemented")
    }
    /* 
    const changeUrl = (key, value, operator) => {

        const root = typeof window !== "undefined" ? window.location.toString().split("?")[0] : ""
        const page = root.split("/")[3]
        const params = typeof window !== "undefined" ? window.location.href.split("?").slice(1)[0] : []
        const params_list = typeof params !== "undefined" ? params.split("\&") : []
        const params_dict = {}
        params_list.map(item => 
            params_dict[item.split("=")[0]] = item.split("=")[1]
        )

        if (operator === "add"){
            params_dict[key] = value
        }
        if (operator === "delete"){
            delete params_dict[key]
        }
        var params_rebuilt = ""
        Object.keys(params_dict).map(key =>
            params_rebuilt += "&" + key + "=" + params_dict[key]
        )
        const params_rebuilt_init = params_rebuilt.replace(/^\&/, "?")

        if (typeof window !== 'undefined'){
            window.history.pushState("aaa", "currentProject", "/" + page + params_rebuilt_init)
        }
    }
    */ 

    const templateZenData = [
        {
            "id": "Template1"
        }
    ]
    // localStorage / json-server
    const zenDataInLocalStorageAtStart = typeof window !== 'undefined' ?  JSON.parse(localStorage.getItem("zenData")) : null
    const initialZenData = zenDataInLocalStorageAtStart === null ? templateZenData : zenDataInLocalStorageAtStart

    const [zenData, setZenData] = useState(initialZenData)

    const updateZenDataWithRemoteData = (newZenData) => {
        setZenData(newZenData)
        if (typeof window !== 'undefined'){ 
            localStorage.setItem("zenData", JSON.stringify(newZenData))
        }
    }

    const go_zen_button_text = "Let's go!"

    const [appsVisible, setAppsVisible] = useState(false)

    const handleAppsClicked = (event) => {
        const newAppsVisible = appsVisible === true ? false : true
        setAppsVisible(newAppsVisible)
    }

    return (

        <div>   

            <Navigation />
            
            <div style={{position:"-webkit-sticky", position:"sticky", top:"0", textAlign: "center"}}>
                <button value="index" onClick={handleGoIndex}>Home</button><button onClick={handleAppsClicked}>Apps</button><a href="/about"><button>Help</button></a> <button>Loupe</button><a href="people?user=jibe-b"><button>People</button></a>
                {
                    appsVisible === true
                    ?
                        <div style={{position:"fixed", right:"5%", top:"10%", zIndex:"2"}}>
                            <p><a href="https://cloud.scenarioB.org">cloud.scenarioB.org</a></p>
                            <p><a href="https://chat.scenarioB.org">chat.scenarioB.org</a></p>
                        </div>
                    :
                        null
                }
            </div>

            { indexOrLive === "live" 
              ?                
                <div>
                    
                    <div style={{position:"-webkit-sticky", position:"sticky", top:"30px"}}>
                        {
                            onAir === "off"
                            ?   
                                
                                <div>
                                    {/* {/* */}
                                    <p style={{texteAlign:"center", backgroundColor:"white"}}>Être notifié·e de la prochaine session de contribution Live ! <button><span role="img"aria-label="Cloche">🔔</span></button></p>
                                    {/* */}
                                </div>
                               
                            : (
                                <div> {/* Il faudra afficher la visio qui corresond à la mission */}
                                    <iframe width="100%" height={currentWindowHeight*0.40} allow="camera; microphone; display;" title="live-jitsi" src="https://framatalk.org/9i936opjjc44ia#config.startWithAudioMuted=true&startWithVideoMuted=false&config.requireDisplayName=true&config.disableDeepLinking=true"></iframe>
                                    { 
                                    showHelpLaunchInBrowser
                                    ?
                                        
                                            currentWindowWidth < 500 
                                            ?
                                                <p onClick={hideMe} style={{textAlign:"right", fontSize:"0.5em", position:"relative", bottom:"60px"}}>Sur mobile, cliquez "Lancer dans le navigateur" <a href="https://github.com/jitsi/jitsi-meet/issues/7612">i</a></p>
                                             : null
                                    : null
                                 }
                                </div>
                            )
                        }
                    </div>

                    <div title="tutos">

                            <h4 style={{textAlign:"center"}}>Comment contribuer</h4>
                            <div style={{textAlign:"center"}}>
                                <iframe title="tuto"
                                    height={currentWindowHeight < currentWindowWidth ? currentWindowHeight*0.6 : currentWindowHeight*0.4 }
                                    width={currentWindowWidth > 800 ? currentWindowWidth * 0.6 : "100%"}
                                    src={
                                        currentURL.search("https*://[^/]*\.gitpod.io") === -1
                                        ?
                                            zenData[currentProject]["tuto_iframe_url"] !== "" ? zenData[currentProject]["tuto_iframe_url"] : "https://scenariob.org/tutos/?mission=liste_des_tutos"
                                        :
                                            backup_tuto_url + "/tutos/?mission=" + zenData[currentProject]["id"]
                                    }></iframe>
                                    <button value={currentProject} onClick={handleClickEdit}>Editer le tutoriel</button>
                            </div>
                        </div>
                    {
                        showWhat === "tuto" 
                        ?

                            <div title="live" style={{textAlign:"center"}}>
                                <button style={{fontSize: "2em"}} onClick={handleShowWhat}>Let's go!</button>
                            </div>
                        :
                            <div title="zen" style={{textAlign: "center"}}>

                                <h4 style={{textAlign:"center"}}>À vous de jouer !</h4>

                                <p style={{textAlign:"center"}}>{zenData[currentProject]["indication_concise"]}</p>
                                
                                <iframe title="zen-input"
                                    height={currentWindowHeight < currentWindowWidth ? currentWindowHeight*0.7 : currentWindowHeight*0.7 }
                                    width={currentWindowWidth > 800 ? currentWindowWidth * 0.8 : "100%"}
                                    src={zenData[currentProject]["zen_iframe_url"]} ></iframe>

                                <h4 style={{textAlign:"center"}}></h4>
                                <div>
                                    {/*Room configurée pour être visible même sans compte*/}
                                    <p>
                                        Svp, connectez-vous/inscrivez-vous pour accéder au chat (possibilité via un compte google).
                                    </p>
                                    <iframe
                                    title="element.io"
                                    width="100%"
                                    height="500px"
                                    src="https://element.tedomum.net/#/room/#scenariob-missiontest:matrix.org"></iframe>
                                </div>

                                {/*

                                Matrix à servir soi-même pour être embedded
                                <iframe title="chat"
                                    height={currentWindowHeight < currentWindowWidth ? currentWindowHeight*0.3 : currentWindowHeight*0.3 }
                                    width={currentWindowWidth > 800 ? currentWindowWidth * 0.8 : "100%"}
                                    src={zenData[currentProject]["chat_iframe_url"]}></iframe>

                                <p>À plein on a un impact colossal !</p>
                        
                                <iframe title="pulse" src={zenData[currentProject]["pulse_iframe_url"]}
                                    width="60%"
                                    heigth={currentWindowHeight * 0.5}
                                ></iframe>
                                {/*  */}
                                <div style={{textAlign:"center"}}><button onClick={handleShowWhat}>Réduire l'interface de contribution</button></div>

                            </div>

                    }
                        
                    <div title="feedback" style={{textAlign:"center"}}>
                        {/*
                        <p>À tout moment, donnez du feedback si vous êtes bloqué·e</p>
                        <div title="feedback-bar">
                            
                            <button>Compris !</button> <button>Je galère</button> <button>Suggestion !</button> <button>Fini ? -> Feedback!</button>
                            
                        </div>
                        */}

                        {/*<iframe title="airtable-feedback" src="https://airtable.com"></iframe>*/}
                    </div>

                    <div style={{paddingTop: "15%"}}></div>
                    <hr />
                    <h3 style={{textAlign: "center"}}>Il y a mille autres manière de contribuer</h3>
                    <div style={{paddingLeft: "35%"}}>
                        <div style={{width: "30%"}}>
                            <hr />
                        </div>
                    </div>

                    <div style={{textAlign: "center"}}>
                    Réalisez la même mission pour un autre projet
                        {/* <button><img src="../images/dropdown.png"></img> Projet</button> */}
                        <div class="dropdown" style={{position: "relative", display: "inline-block"}}>
                            <button class="dropbtn" style={{backgroundColor: "#4CAF50", color: "white", padding: "16px", fontSize: "16px", border: "none", cursor: "pointer"}}>Dropdown</button>
                            <div class="dropdown-content" style={{display: "none", position: "absolute", backgroundColor: "#f9f9f9", minWidth: "160px", boxShadow: "0px 8px 16px 0px rgba(0,0,0,0.2)", zIndex: "1"}}>
                                <div style={{color: "black", padding: "12px 16px", textDecoration: "none", display: "block"}}>
                                    <a href="#" >Link 1</a>
                                    <a href="#">Link 2</a>
                                    <a href="#">Link 3</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div style={{paddingBottom:"30px"}}></div>

                    <div title="footer-cagnotte" style={{position:"fixed", bottom:"0px", backgroundColor:"white", paddingLeft: currentWindowWidth > 800 ?"38%" : 0}}>
                            <a href="https://opencollective.com/contribuons">[XXXXXXXXXX.......] Cagnotte contribution</a>
                    </div>


                </div>
              : null }

            <div>

                <div>
                    <button>Mastodon</button><button>Flux de contribution</button>
                </div>
                <div title="creator">
                    Quelle mission voulez-vous décrire ? <input></input>
                    <iframe title="creator-tuto" src="https://example.com/airtable.com"></iframe>
                </div>

                <div>

                    <div style={{paddingLeft: currentWindowWidth > 800 ? "30%" : 0, paddingRight: currentWindowWidth > 800 ? "30%" : 0}}>
                    
                    {
                        zenData.map((item, index) => 
                            <div>
                                {
                                    parseInt(currentProject) !== index
                                    ?
                                        <div>
                                        <p>{item["id"]}</p>
                                        <iframe title={item["id"]}
                                        src={
                                            currentURL.search("https*://[^/]*\.gitpod.io") === -1
                                            ?
                                                zenData[index]["tuto_iframe_url"] !== "" ? zenData[index]["tuto_iframe_url"] : "https://scenariob.org/tutos/?mission=liste_des_tutos"
                                            :
                                                backup_tuto_url + "/tutos/?mission=" + zenData[index]["id"]
                                        }></iframe>
                                        <button value={index} onClick={handleIndexOrLive}>{go_zen_button_text}</button>
                                        <div>
                                            {/*
                                            <button>J'adore !</button><button>(?)</button>
                                            */}
                                        </div>
                                    </div>
                                    : null
                                }
                            </div>
                        )
                    }

                    {/*
                    <div>
                        <p>Ça te dis une notification pour te ramener ici ? Dans combien de temps ?</p>
                        <input></input> minutes<button>Go</button>
                    </div>
                    */}
                        
                    </div>
                )
                
                </div>
            </div>
        </div>

    )
}
