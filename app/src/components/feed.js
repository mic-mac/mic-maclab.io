import React from "react"

export default (props) => {

    const handleClickZen = (event) => {
        //props.setCurrentMission(event.target.value)
        window.location.href = "/?mission=" + event.target.value
    }

    return(
        <div>
            <div style={{textAlign: "left"}}>
                {
                    props.posts.map(
                        item => 
                        <div>
                            {
                            props.extractPostsTypesToShow(props.location).includes(item.type)
                            ?
                                <button value={item.id} onClick={handleClickZen} style={{backgroundColor: "white", border: "0px", width: "100%"}}>
                                    <div><button style={{backgroundColor: "white", border: "0px", width: "100%", textAlign: "left"}} value={item.id}>{item.author} {item.creationDate}</button></div>
                                    <div><button style={{backgroundColor: "white", border: "0px", width: "100%", textAlign: "left"}} value={item.id}>{item.text}</button></div>
                                    <div>
                                        <button style={{position: "absolute", height: props.currentScreenSize.height / 3, border: "0px", width: "100%", opacity: "0%"}} value={item.id}></button>
                                        <iframe
                                            title="vignette"
                                            src={item.link}
                                            width="100%"
                                            height={props.currentScreenSize.height / 3}
                                        >
                                        </iframe></div>
                                        <div style={{paddingLeft: "10%", paddingRight: "10%"}}>
                                            <button style={{position: "absolute", height: props.currentScreenSize.height / 5, border: "0px", width: "48%", opacity: "0%"}} value={item.id}></button>
                                            <iframe
                                                title="chat"
                                                src={item.chat}
                                                width="100%"
                                                height={props.currentScreenSize.height / 5}
                                            >
                                            </iframe>
                                        </div>
                                </button>
                            :
                                null
                            }
  
                        </div>
                    )
                }

            </div>

            {/*
            <div style={{textAlign:"center"}}>    
                <div>
                    <iframe title="mastofeed" allowfullscreen sandbox="allow-top-navigation allow-scripts"
                    width="70%"
                    height={props.currentScreenSize.height / 1.5}
                    src="https://www.mastofeed.com/apiv2/feed?userurl=https%3A%2F%2Fgroups.qoto.org%2Fusers%2Fmegaphone&theme=auto&size=100&header=false&replies=false&boosts=false">
                    </iframe>
                </div>

                <div>
                    <iframe title="mastofeed-scenariob" allowfullscreen sandbox="allow-top-navigation allow-scripts"
                    width="70%"
                    height={props.currentScreenSize.height / 1.5}
                    src="https://www.mastofeed.com/apiv2/feed?userurl=https%3A%2F%2Fgroups.qoto.org%2Fusers%2Fscenariob&theme=auto&size=100&header=false&replies=false&boosts=false">
                    </iframe>
                </div>

            </div>
            */}

        </div>
    )
}