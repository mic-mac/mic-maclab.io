import React, { useState } from "react"
import Target from "../components/target"

export default (props) => {

    const initialMission = typeof window != 'undefined' ? window.location.href.split("?m=")[1] : "no-mission-specified"
    const [mission,] = useState(initialMission)

    return(
        <div>
            <Target 
                mission={mission}
            />
        </div>
    )
}