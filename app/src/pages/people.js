import React, { useLayoutEffect, useState } from "react"


export default () => {

    const url = typeof window !== "undefined" ? window.location.toString() : ""
    const user = url.split("?user=")[1]

    function useWindowSize() {
        const [size, setSize] = useState([0, 0]);
        useLayoutEffect(() => {
            function updateSize() {
            setSize([window.innerWidth, window.innerHeight]);
            }
            window.addEventListener('resize', updateSize);
            updateSize();
            return () => window.removeEventListener('resize', updateSize);
        }, []);
        return size;
    }
    
    function ShowWindowDimensions(props) {
    const [width, height] = useWindowSize()
    return {width: width, height:height}
    }
    
    const currentSize = ShowWindowDimensions()
    //const currentWindowWidth = currentSize.width
    
    const currentWindowHeight = currentSize.height

    return (
        <div>
            <iframe title={user + ".gitlab.io"}
                src={"https://" + user + ".gitlab.io"}
                width="100%"
                height={currentWindowHeight - 100 }
            ></iframe>
            <p>
                Vous faites partie de Scénario B ? Publiez votre page sur votre-nom.gitlab.io, votre page sera alors accessible à <a href="https://mic-mac.gitlab.io/people?user=votre-nom">https://mic-mac.gitlab.io/people?user=votre-nom</a>
            </p>
        </div>
    )
}