[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/mic-mac/mic-mac.gitlab.io)

Pour suivre les étapes de ce projet, consultez l'historique ([sur gitlab](https://gitlab.com/jibe-b/micmac/-/commits/master), en ligne de commande `git log`)

## Features

gatsby : l'appli est une PWA générée par Gatsbyjs pour permettre une contribution au code aisée, même pour des non-expert·e·s.

Pour consulter la liste des features en développement, consultez la liste des branches ([sur gitlab](https://gitlab.com/jibe-b/micmac/-/branches), en ligne de commande `git branch`)

## Requirements

node doit être au moins à la version 12 pour permettre à gatsby d'être utilisé.

Aside: https://github.com/hedgedoc/cli
