
import React, {useState} from "react"
import axios from "axios"

export default (props) => {
    
    const [form_data,] = useState({
        name: "Through proxy",
        fullAddress: "Rome",
        latitude: 300,
        longitude: 300,
        postalCode: "",
        addressLocality: "",
        streetAddress: "",
        addressCountry: "",
        customFormatedAddress: "",
        "category-field-1": "",
        "category-field-4": "",
        "category-field-6": "",
        "category-field-7": "",
        "options-values": [{}],
        checkbox_group_1610024867057: "",
        textarea_1609946064902: "",
        textarea_1609946188447: "",
        text_1609946180665: "",
        number_1609946217499: "",
        text_1609946222800: "",
        text_1609946246486: "",
        agree: "",
        "submit-option": "backtomap"
    })
    

    const post = (form_data) =>{

        console.log(form_data)

        axios.post("http://localhost:8765/https://demolistras.gogocarto.fr/elements/add", {
            "headers": {
                "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*REMOVE_THIS/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
                "accept-language": "en-US,en;q=0.9,fr;q=0.8",
                "cache-control": "max-age=0",
                "content-type": "multipart/form-data; boundary=----WebKitFormBoundaryWA8tjTBRkDTlrN1j",
                "sec-fetch-dest": "document",
                "sec-fetch-mode": "navigate",
                "sec-fetch-site": "same-origin",
                "sec-fetch-user": "?1",
                "upgrade-insecure-requests": "1",
                "cookie": "firstVisit=done; _pk_id.13.7078=04e79d5e009d2319.1611736470.; address=; _pk_ses.13.7078=1; viewport=@38.895,-77.036,12z"
            },
            "referrer": "https://demolistras.gogocarto.fr/elements/add",
            "referrerPolicy": "strict-origin-when-cross-origin",
            "data": "------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"element[fullAddress]\"\r\n\r\nRome\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"element[geo][latitude]\"\r\n\r\n0\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"element[geo][longitude]\"\r\n\r\n0\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"element[address][postalCode]\"\r\n\r\n\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"element[address][addressLocality]\"\r\n\r\n\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"element[address][streetAddress]\"\r\n\r\n\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"element[address][addressCountry]\"\r\n\r\n\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"element[address][customFormatedAddress]\"\r\n\r\nWashington\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"category-field-1\"\r\n\r\n27\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"category-field-4\"\r\n\r\n12\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"category-field-6\"\r\n\r\n23\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"category-field-7\"\r\n\r\n28\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"options-values\"\r\n\r\n[{\"id\":\"27\",\"index\":\"1\",\"description\":\"\"},{\"id\":\"12\",\"index\":\"2\",\"description\":\"\"},{\"id\":\"23\",\"index\":\"3\",\"description\":\"\"},{\"id\":\"28\",\"index\":\"4\",\"description\":\"\"}]\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"element[name]\"\r\n\r\nATTEMPT\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"data[checkbox_group_1610024867057][]\"\r\n\r\noption-1\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"data[textarea_1609946064902]\"\r\n\r\nMATERIAUX\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"data[textarea_1609946188447]\"\r\n\r\nTENUE\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"data[text_1609946180665]\"\r\n\r\nEMAIL\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"data[number_1609946217499]\"\r\n\r\n999999999999\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"data[text_1609946222800]\"\r\n\r\nTELEPHONE\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"data[text_1609946246486]\"\r\n\r\nA@A.COM\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"agree\"\r\n\r\non\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j\r\nContent-Disposition: form-data; name=\"submit-option\"\r\n\r\nbacktomap\r\n------WebKitFormBoundaryWA8tjTBRkDTlrN1j--\r\n",
            "method": "POST",
    //     "mode": "cors"
        }).then((response) => {
            console.log(response);
        }).catch((error) => {
            console.log(error);
        })

        
        }

    const handleButtonClicked = (event) => {
        //const form_data = JSON.parse(event.target.value)
        //console.log(form_data)
        post(0) //form_data)
    }

    return(
        <div>

            Target

            <button value={form_data.toString()} onClick={handleButtonClicked}>Create element</button>

            
            
        </div>
    )
}