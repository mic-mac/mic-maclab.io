import React from "react"

export default () => {

    posts = [
        {
            author: "ecrivain",
            creation_date: "01/07/2021",
            text: "Post blabla",
            pj: []
        }
    ]
    const display_post = (post) => {
        if (post.format === "text"){
            return (
                <div>
                    <p>{post.author} {post.creation_date}</p>
                    <p>{post.text}</p>
                </div>
            )
        }
        if (post.format === "image") {
            return(
                <img alt="img" src={post.text}></img>
            )
        }
    }
    return (
        <div style={{textAlign:"center", maxWidth:"80%"}}>

            <div>
                {
                    posts.map(                        post => {
                            <div>
                                {display_post(post)}
                                    {post.pj.map(pj=>(
                                        <div style={{paddingLeft:"20%"}}>
                                                {display_post(posts[pj])}
                                        </div>
                                    )   
                                )}              
                            </div>


                        }
                    )   
                }
            </div>


            <p><img alt="" src="https://gitlab.com/mic-mac/mic-mac.gitlab.io/-/raw/master/app/src/images/scenariob.png"></img></p>
            <p><img alt="" src="https://gitlab.com/mic-mac/mic-mac.gitlab.io/-/raw/master/app/src/images/scenariob-economie-militante.png"></img></p>
            <p><a href="https://opencollective.com/scenariob">Abonnez-vous !</a></p>
            <p max-width="100%"><a href="https://www.facebook.com/groups/258234471940957">Groupe facebook <strong>Redirection coopérative de la consommation</strong></a></p>
            <p><a href="economie-militante">Être recontactée pour en savoir plus sur la dynamique de groupes locaux</a></p>
            <p><a href="https://www.facebook.com/groups/293591535161208/">Mégaphone de l'économie militante</a></p>
            <p><a href="https://www.facebook.com/groups/2171571459596257/">Mégaphone des initiatives collectives</a></p>
            <p><a href="https://www.facebook.com/groups/470511086490229/">Mégaphone des gestes écolos quotidiens</a> (actuellement via le groupe Éco-colocation)</p>
            <p><a href="mailto:jibe.bohuon@scenariob.org">Contact</a></p>
        </div>
    )
}
