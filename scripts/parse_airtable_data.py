# -*-coding:utf-8 -*

from os import system, environ

airtable_api_key = environ['AIRTABLE_API_KEY']

directory_path = "app/src/data"
data_types = ["Categories-Missions", "Missions", "Categories-Recettes", "Recettes", "Niveaux", "Organisations"]


for data_type in data_types:
	system("curl https://api.airtable.com/v0/app0sKi1X4q6xMyVG/{}?api_key={} | sed 's_^_[_;s_$_]_' | jq ''> {}/{}.json".format(data_type, airtable_api_key, directory_path, data_type))
	
	if data_type == "Missions":
		system("echo $(echo [ ;curl http://51.178.183.220:3001/tutos ; echo ] ) > {}/Recommended-{}.json".format(directory_path, data_type))
