import React from "react"

export default (props) => {

    const handleButtonClicked = (event) => {
        props.setCurrentView(event.target.value)
    }

    return(
        <div>
            {
                [
                    {text: "Guide", value: "0"},
                    {text: "Action", value: "1"},
                    {text: "Results", value: "2"}
                ].map(
                    item => 
                        <span style={{paddingRight: "1%"}}>
                            <button value={item.value} onClick={handleButtonClicked} style={{height: "50px", width:"25%"}}>
                                {item.text}
                                <button style={{position: "absolute", height: "50px", border: "0px", width: "25%", opacity: "0%"}}></button>
                                <iframe title="viewButtonRole" height="100%" width="100%" src={"https://pad.lamyne.org/p/vK-XHeZoRrKK72oe3DT5qg#/" + item.value}></iframe>
                            </button>
                        </span>
                )
            }
            
        </div>
    )
}