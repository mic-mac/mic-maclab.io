import React, { useState } from "react"
import ChatButtons from "./chatButtons"
import BelowView from "./belowView"

export default (props) => {

    const [currentView, setCurrentView] = useState("chat")

    return(
        <div>
                <ChatButtons
                    setCurrentView={setCurrentView}
                />
                <BelowView
                    currentScreenSize={props.currentScreenSize}
                    posts={props.posts}
                    postsIndex={props.postsIndex}
                    currentMission={props.currentMission}
                    currentView={currentView}
                />
            </div>
    )
}