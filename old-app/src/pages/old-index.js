
import React, { useState } from "react"
import { graphql } from "gatsby"

import AppLayout from "../components/layout"
import Navigation from "../components/app-navigation"
import MainDisplay from "../components/app-main-display"
import AppSearchBar from "../components/SearchBar"
import CategoryTabs from "../components/category-tabs"

import Next from "../components/next"

import '../utils/i18n'; // initialisation de i18next
import { withTranslation } from "react-i18next";

// import { set } from "mongoose"
// import { PromiseProvider } from "mongoose"

const App = ( { data }) => {

  const [ displayComponentsDebug, setDisplayComponentsDebug ] = useState(false)
  // const [ site, setSite ] = useState(location.href.toString().split('//')[1].split(':')[0])
  const [ screenWidth, ] = useState(1200) //window.innerWidth) setScreenWidth
  const isDesktop = screenWidth > 500

  const [ visibleDisplay, setVisibleDisplay ] = useState("home")
  const [ queryInApp, setQueryInApp ] = useState("")
  const [ clicksCountTracking, setClicksCountTracking ] = useState(0)
  const [ selectedTuto, setSelectedTuto ] = useState("")
  const [ displaySearchSuggestionsMenu, setDisplaySearchSuggestionsMenu ] = useState(false)
  const [ displaySubscribeForm, setDisplaySubscribeForm] = useState(false)
  const [ results, setResults ] = useState({ query: "", results: data.allMissionsJson.edges[0].node })

  const indexObject = (object) => {
      const output = {}

      var index = 0
      for ( const key of object.map(item => item.id) ) {
          output[key] = object[index]
          index = index + 1
      }
      return output
  }

  const missions = { 
      query,
      results : indexObject(data.allMissionsJson.edges[0].node.records)
  }

  const organisations = indexObject(data.allOrganisationsJson.edges[0].node.records)

  const recommended = {
    results: indexObject(data.allRecommendedMissionsJson.edges[0].node.records)
  }

  const [ recommendations, setRecommendations ] = useState("") // recommendations
  const [ recommendationsToEdit, setRecommendationsToEdit ] = useState(missions) // recommendations

  // this is pretty awful
  const handleSetRecommendations = (recommended) => {
    setRecommendations(recommended)
    setRecommendationsToEdit(recommended)
  }
  if (recommendations === "") handleSetRecommendations(recommended)
  //hopefully it's short enough

  const handleSearch = () => {
      retrieveTutos(queryInApp)
      setVisibleDisplay("results")
      setQueryInApp("")
      setDisplaySearchSuggestionsMenu(false)

      setClicksCountTracking(clicksCountTracking + 1)
  }

  const retrieveTutos = (queryInApp) => {
      setResults(missions)
  }

  const handleKeyUp = (event, queryInApp) => {
      setQueryInApp(event.target.value)
      if ( queryInApp.length > 2 ) setDisplaySearchSuggestionsMenu(true) 
      else setDisplaySearchSuggestionsMenu(false)
      if (event.keyCode === 13) handleSearch(event)
  }
  
  const handleSuggestionClick = event => {
      handleClickTuto(event)
      setDisplaySearchSuggestionsMenu(false)
  }
  
  const handleClickTuto = (event) => {
      retrieveTutos(queryInApp)
      setSelectedTuto(event.target.value)
      setVisibleDisplay("tutos")
  }

  const handleRecommentationClick = (event) => {
      handleClickTuto(event)
  }

/*   const data_categories = site === "agirpourun.scenariob.org" 
    ? data.allCategoriesMissionsJson
    : site === "solutions-ecolos.scenariob.org"                              
      ? data.allCategoriesRecettesJson
      : site === "localhost"
        ? data.allCategoriesMissionsJson // data.allCategoriesRecettesJson
        : null */
  const data_categories = data.allCategoriesMissionsJson

  const categories = data_categories.edges[0].node.records 

  const niveaux = data.allNiveauxJson.edges[0].node.records

  const handleCategoryClick = (event) => {
      setQueryInApp(event.target.value)
      handleSearch()
  }

  const handleDisplaySubscribeForm = (event) => {
      setDisplaySubscribeForm(true)
  }

  return (
      <div>
        {
          visibleDisplay === "next" ? <Next suggestions={results} setVisibleDisplay={setVisibleDisplay} handleClickTuto={handleClickTuto} /> :

          (isDesktop ? (
              <AppLayout>
                  <Navigation setVisibleDisplay={setVisibleDisplay} displayComponentsDebug={displayComponentsDebug} setDisplayComponentsDebug={setDisplayComponentsDebug} />

                  <table>
                      <tbody>
                          <tr>
                              <td style={{ width: "12%", verticalAlign: "top"}}>
                                  <CategoryTabs
                                    categories={categories}
                                    niveaux={niveaux}
                                    handleCategoryClick={handleCategoryClick}
                                    displayComponentsDebug={displayComponentsDebug}
                                    setDisplayComponentsDebug={setDisplayComponentsDebug}

                                  />
                              </td>
                              <td style={{ textAlign: "center", verticalAlign: "top"}}>
                                    
                                  <div>
                                      <AppSearchBar
                                          displaySearchSuggestionsMenu={displaySearchSuggestionsMenu}
                                          setDisplaySearchSuggestionsMenu={setDisplaySearchSuggestionsMenu}
                                          handleSearch={handleSearch}
                                          handleClickTuto={handleClickTuto}
                                          handleSuggestionClick={handleSuggestionClick}
                                          visibleDisplay={visibleDisplay}
                                          setVisibleDisplay={setVisibleDisplay}
                                          queryInApp={queryInApp}
                                          handleKeyUp={handleKeyUp}
                                          suggestions={results}
                                          displayComponentsDebug={displayComponentsDebug}
                                          setDisplayComponentsDebug={setDisplayComponentsDebug}
                                          
                                          position={ visibleDisplay === "home" ? "middle" : "high" }
                                      />

                                      {/*<GridSuggestions categories={categories} />*/}

                                      
                                  </div>
                                        
                                  <MainDisplay
                                      visibleDisplay={visibleDisplay}
                                      selectedTuto={selectedTuto}
                                      handleClickTuto={handleClickTuto}
                                      results={results}
                                      isDesktop={isDesktop}
                                      recommendations={recommendations}
                                      handleRecommentationClick={handleRecommentationClick}
                                      queryInApp={queryInApp}
                                      recommendationsToEdit={recommendationsToEdit}
                                      organisations={organisations}
                                      displayComponentsDebug={displayComponentsDebug}
                                      setDisplayComponentsDebug={setDisplayComponentsDebug}

                                      displaySubscribeForm={displaySubscribeForm}
                                      handleDisplaySubscribeForm={handleDisplaySubscribeForm}
                                  />
                                      
                              </td>
                              {
                                  visibleDisplay === "tutos" ? null : <td style={{ width: "12%", verticalAlign: "top"}}></td> 
                              }                               
                          </tr>
                      </tbody>

                  </table>

                  </AppLayout>
                                  ) : (<p>soon on mobile!</p>)
                              
  )   }                   
      </div>
      )
}


export const query = graphql`
  query {
    allMissionsJson {
      totalCount
      edges {
        node { 
            records {
                id
                fields {
                    Titre
                    Description_r_sum_e
                    Author
                    Slides_url
                    S_quence
                    Cat_gorie
                    Niveau
                    Responsables
                }
            }
        }
      }
    },
    allRecommendedMissionsJson {
      totalCount
      edges {
        node { 
            records {
                id
                fields {
                    Titre
                    Description_r_sum_e
                    Author
                    Slides_url
                    S_quence
                    Cat_gorie
                    Niveau
                    Responsables
                }
            }
        }
      }
    },
    allCategoriesMissionsJson {
      totalCount
      edges {
        node { 
            records {
                id
                fields {
                    Name
                    Parent
                }
            }
        }
      }
    },
    allRecettesJson {
      totalCount
      edges {
        node { 
            records {
                id
                fields {
                    Titre
                    Author
                    Slides_url
                    S_quence
                    Cat_gorie
                    Niveau
                }
            }
        }
      }
    },
    allCategoriesRecettesJson {
      totalCount
      edges {
        node { 
            records {
                id
                fields {
                    Name
                    Parent
                }
            }
        }
      }
    },
    allNiveauxJson {
      totalCount
      edges {
        node { 
            records {
                id
                fields {
                    Name
                    url
                }
            }
        }
      }
    },
    allOrganisationsJson {
      totalCount
      edges {
        node { 
            records {
                id
                fields {
                    Name
                    url
                }
            }
        }
      }
    }
  }
`

export default withTranslation()(App)


/*,
    allNiveauxJson {
      totalCount
      edges {
        node {
          id
          Name
          url
        }
      }
    }
    allMissionsJson {
      totalCount
      edges {
        node {
          id
          Titre
          Author
          Slides_url
          S_quence
          Cat_gorie
          Niveau
        }
      }
    }
    allOrganisationsJsonpkill firefox
     {
      totalCount
      edges {
        node {
          id
          Name
          url
        }
      }
    }
    */