import React, { useState } from "react"
//import Axios from "axios"
import * as JsSearch from "js-search"

/* <input style={{width: "60%"}} query={props.queryInApp} placeholder="Rechercher des tutos…" onKeyDown={handleKeyUp} />
                        <button query={props.queryInApp} onClick={props.handleSearch}>Rechercher</button> */
export default props => {
  /**
   * React lifecycle method to fetch the data
   */

    /* async componentDidMount() {

    Axios.get("http://vps820264.ovh.net:3001/tutos")
      .then(result => {
        const tutosData = result.data.records.map(
          item => {
            const description = item.fields["Description résumée"] ? item.fields["Description résumée"] : ""
            const author = item.fields["Author"] ? item.fields["Author"] : ""
            return(
            
              {
                id: item.id,
                Titre: item.fields["Titre"],
                Description: description,
                Author: author
              }
            
            )
          }
        )

        this.setState({ tutosList: tutosData })
        this.rebuildIndex()

      })
      .catch(err => {
        this.setState({ isError: true })
        console.log("====================================")
        console.log(`Something bad happened while fetching the data\n${err}`)
        console.log("====================================")
      })
  }   */

/*     async componentDidMount() {
      
      new Promise(function (resolve, reject){resolve("done")}).then(_ =>

        { */

  const tutosData = props.suggestions.results.records.map(
    item => {
      const description = item.fields["Description_r_sum_e"] ? item.fields["Description_r_sum_e"] : ""
      const author = item.fields["Author"] ? item.fields["Author"] : ""
      return(
        {
          id: item.id,
          Titre: item.fields["Titre"],
          Description: description,
          Author: author
        }
      )
    }
  )

  const [state, setState] = useState(
    {
      tutosList: tutosData,
      search: [],
      searchResults: [],
      isLoading: true,
      isError: false,
      searchQuery: "",
      displayResults: false,
    }
  )

  const rebuildIndex = (state) => {
    const { tutosList } = state

    console.log(tutosList)

    if (! tutosList) alert("Undefined")

    const dataToSearch = new JsSearch.Search("id")
    /**
     *  defines a indexing strategy for the data
     * more about it in here https://github.com/bvaughn/js-search#configuring-the-index-strategy
     */
    dataToSearch.indexStrategy = new JsSearch.PrefixIndexStrategy()
    /**
     * defines the sanitizer for the search
     * to prevent some of the words from being excluded
     *
     */
    dataToSearch.sanitizer = new JsSearch.LowerCaseSanitizer()
    /**
     * defines the search index
     * read more in here https://github.com/bvaughn/js-search#configuring-the-search-index
     */
    dataToSearch.searchIndex = new JsSearch.TfIdfSearchIndex("id")
    dataToSearch.addIndex("Titre") // sets the index attribute for the data
    //dataToSearch.addIndex("Description") // sets the index attribute for the data
    //dataToSearch.addIndex("Author") // sets the index attribute for the data
    dataToSearch.addDocuments(tutosList) // adds the data to be searched
    setState({ search: dataToSearch, isLoading: false })
  }

  var alreadyDone = false
  if (alreadyDone === false) {
    rebuildIndex(state)
    alreadyDone = true
  }



  /**
   * handles the input change and perform a search with js-search
   * in which the results will be added to the state
   */
  const searchData = e => {
    if (state.searchQuery.length > 2) {setState( {...state, displayResults: true} )}
    const { search } = state
    const queryResult = search.search(e.target.value)
    setState({ searchQuery: e.target.value, searchResults: queryResult })
  }
  const handleSubmit = e => {
    e.preventDefault()
    //props.setVisibleDisplay("results")
  }

  const handleClickTuto = e => {
    props.handleClickTuto(e)
    setState( { ...state, displayResults: false})
  }

  
    const { tutosList, searchResults, searchQuery } = state
    const queryResults = searchQuery === "" ? tutosList : searchResults
    return (
      <div>
       
        <div style={{ margin: "0 auto" }}>

        <p>State : {Object.keys(state.tutosList).length ? Object.keys(state.tutosList).length : "not ready"}</p>

          <form onSubmit={handleSubmit}>
            <div style={{ margin: "0 auto" }}>
              <label htmlFor="Search" style={{ paddingRight: "10px" }}>
                Enter your search here
              </label>
              <input
                id="Search"
                value={searchQuery}
                onChange={searchData}
                placeholder="Enter your search here"
                style={{ margin: "0 auto", width: "400px" }}
              />
            </div>
          </form>
          {
            state.displayResults === false
            ? null
            : (
              <div>
                Number of items:
                {queryResults.length}
                <table
                  style={{
                    width: "100%",
                    borderCollapse: "collapse",
                    borderRadius: "4px",
                    border: "1px solid #d3d3d3",
                  }}
                >
                  <thead style={{ border: "1px solid #808080" }}>
                    <tr>
                      <th
                        style={{
                          textAlign: "left",
                          padding: "5px",
                          fontSize: "14px",
                          fontWeight: 600,
                          borderBottom: "2px solid #d3d3d3",
                          cursor: "pointer",
                        }}
                      >
                        Titre
                      </th>
                      <th
                        style={{
                          textAlign: "left",
                          padding: "5px",
                          fontSize: "14px",
                          fontWeight: 600,
                          borderBottom: "2px solid #d3d3d3",
                          cursor: "pointer",
                        }}
                      >
                        Description
                      </th>
                      <th
                        style={{
                          textAlign: "left",
                          padding: "5px",
                          fontSize: "14px",
                          fontWeight: 600,
                          borderBottom: "2px solid #d3d3d3",
                          cursor: "pointer",
                        }}
                      >
                        Auteurs
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {queryResults.map(item => {
                      return (
                        <tr key={`row_${item.id}`}>
                          <td
                            style={{
                              fontSize: "14px",
                              border: "1px solid #d3d3d3",
                            }}
                          >
                            <button value={item.id} onClick={handleClickTuto}>{item.Titre}</button>
                          </td>
                          <td
                            style={{
                              fontSize: "14px",
                              border: "1px solid #d3d3d3",
                            }}
                          >
                            {item.Description}
                          </td>
                          <td
                            style={{
                              fontSize: "14px",
                              border: "1px solid #d3d3d3",
                            }}
                          >
                            {item.Author}
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                </table>
              </div>
            )
          }
        </div>
      </div>
    )
  }