import React from "react"
import Live from "./live"
import Target from "./target"
import Recommendations from "./recommendations"

export default (props) => {

    return(
        <div>
            <Live
                current_display_infos={props.current_display_infos}
            />
            <Target
                mission={props.mission}
                missions={props.missions}
                current_display_infos={props.current_display_infos}
            />
            <Recommendations 
                recommendations={props.recommendations}
            />
        </div>
    )
}