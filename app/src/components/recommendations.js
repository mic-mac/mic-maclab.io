
import React from "react"

export default (props) => {

    return(

        <div>
            Recommendations
            <table>
                <thead></thead>
                <tbody>

                    {
                        Object.keys(props.recommendations).map(key =>
                            <tr>
                                <td style={{width:"15%"}}>
                                    <iframe
                                        title="tuto-xyz"
                                        src={props.recommendations[key].tuto}
                                    ></iframe>
                                </td>
                                <td style={{textAlign:"left"}}>
                                    <a href={"/?mission=" + key}>{props.recommendations[key].title}</a>
                                </td>
                            </tr>
                        )
                    }
                </tbody>
                
            </table>
        </div>
    )
}


